export CLICOLORS=1
if type -p dircolors >/dev/null; then
    [ -f ~/.dircolors ] && eval $(dircolors -b ~/.dircolors)
fi

. ~/.bash-prompt

if [ -t 0 -a -t 1 ]; then
    #kill flow control
    stty -ixon
    stty -ixoff
    if [ ${BASH_VERSINFO[0]} -ge 4 ]; then
        shopt -s cdspell
        shopt -s extglob
        shopt -s cmdhist
        shopt -s checkwinsize
        shopt -s no_empty_cmd_completion
        shopt -u promptvars
        shopt -s histappend
        set -o noclobber
        shopt -s dirspell
        #don't echo ^C
        stty -ctlecho
    fi
fi

if [ "$TERM" = "linux" ]; then
    #Black / Light black
    echo -en "\e]P0222222"
    echo -en "\e]P8666666"
    #Red / Light red
    echo -en "\e]P1cc4747"
    echo -en "\e]P9bf5858"
    #Green / Light green
    echo -en "\e]P2a0cf5d"
    echo -en "\e]PAb8d68c"
    #Yellow / Light yellow
    echo -en "\e]P3e0a524"
    echo -en "\e]PBedB85c"
    #Blue / Light blue
    echo -en "\e]P44194d9"
    echo -en "\e]PC60aae6"
    #Purple / Light purple
    echo -en "\e]P5cc2f6e"
    echo -en "\e]PDdb588c"
    #Cyan / Light cyan
    echo -en "\e]P66d878d"
    echo -en "\e]PE42717b"
    #White / Light white...?
    echo -en "\e]P7dedede"
    echo -en "\e]PFf2f2f2"

     #this is an attempt at working utf8 line drawing chars in the linux-console
#    export TERM=linux+utf8
    clear #hmm, yeah we need this or else we get funky background collisions
fi

if [ "$(uname -s)" = "Darwin" ]; then
    alias grep="grep --color=auto"
    alias ls="ls -hFG"
    alias ll="ls -l"
    alias la="ls -a"
    alias x="bsdtar xvf"
else
    alias pacman="pacman --color=auto"
    alias grep="grep --color=auto"
    alias ls="ls -hF --color"
    alias ll="ls -l"
    alias la="ls -a"
    alias x="bsdtar xvf"
fi
alias dc="docker compose"
alias dcr="docker compose run"
alias per="pipenv run"

export NVM_DIR="$HOME/.nvm"
export HISTIGNORE="&:[bf]g:exit"
export HISTCONTROL=ignoredups
export HISTFILESIZE=10000
export HISTSIZE=10000
export INPUTRC=~/.inputrc
export EDITOR=vim
export VISUAL=vim
export HGEDITOR=hgeditor
if [ "$(uname)" = "Darwin" ]; then
  export BROWSER=open
else
  export BROWSER=firefox
fi
export PAGER=less
export MANPAGER=less
export IGNOREEOF=3
export PYTHONSTARTUP=~/.pythonrc
export MYSQL_PS1="(\u@\h) [\d]>\_"

# less man page colors
export GROFF_NO_SGR=1
export LESS="FRSXQ"
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;30m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'
export LESS_TERMCAP_ue=$'\E[0m'

function chkpath () {
    if [ -d "$1" ]; then
        if [[ ! ":$PATH:" =~ ":$1:" ]]; then
            export PATH="$1:$PATH"
        fi
    fi
}

function chksource () {
    if [ -f "$1" ]; then
        . "$1"
    fi
}

export PATH=""
chkpath "/sbin"
chkpath "/usr/sbin"
chkpath "/bin"
chkpath "/usr/bin"
chkpath "/usr/local/sbin"
chkpath "/usr/local/bin"
chkpath "$HOME/.local/bin"
chkpath "$HOME/bin"
chkpath "$HOME/scripts"

# Bash Completion
chksource "/usr/share/bash-completion/bash_completion"

BREWROOT="/opt/homebrew"
if [ -d "$BREWROOT" ]; then
    chkpath "$BREWROOT/bin"
    chkpath "$BREWROOT/sbin"
    chkpath "$BREWROOT/opt/fzf/bin"
    chkpath "$BREWROOT/opt/util-linux/bin"
    chkpath "$BREWROOT/opt/util-linux/sbin"
    chkpath "$BREWROOT/opt/curl/bin"

    chksource "$BREWROOT/opt/bash-completion/etc/bash_completion"

    chksource "$BREWROOT/opt/nvm/nvm.sh" # node
    chksource "$BREWROOT/opt/fzf/shell/completion.bash"
    chksource "$BREWROOT/opt/nvm/etc/bash_completion.d/nvm"
fi

# ruby
if [ -d "$HOME/.cargo" ]; then
  chkpath "$HOME/.cargo/bin"
  chksource "$HOME/.cargo/env"
fi

# rust
chkpath "$HOME/.rbenv/shims"

# python
if type -p pyenv >/dev/null; then
  export PYENV_ROOT="$HOME/.pyenv"
  chkpath "$PYENV_ROOT/bin"
  eval "$(pyenv init --path)"
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"
fi

if type -p pipenv >/dev/null; then
    eval "$(_PIPENV_COMPLETE=bash_source pipenv)"
fi

if type -p docker-compose >/dev/null; then
  chksource ~/bin/docker-stuff
fi

chksource ~/bin/grow-things

if type -p vim >/dev/null; then
    alias vi="vim"
else
    alias vim="vi"
fi

if type -p fzf >/dev/null; then
  alias fzf="fzf --multi --filepath-word"
  alias fzv="vim -p \$(fzf)"
fi

# try to load ssh-agent
if [ -t 0 -a -t 1 -a -z "$SSH_AUTH_SOCK" ]; then
    if type -p ssh-agent >/dev/null; then
        eval $(/usr/bin/ssh-agent -s)
        ssh-add >/dev/null
        trap "eval \$(/usr/bin/ssh-agent -k)" 0
    fi
fi

# Start X if logging in on vt1
if [ -z "$DISPLAY" -a "$XDG_VTNR" = "1" ]; then
    set +o noclobber
    exec startx &> ~/.xlog-$XDG_VTNR
fi

#! /bin/sh

for d in ~/.vim/bundle/*; do
    if [ -d "$d" ]; then
        git submodule update --init "$d"
        pushd "$d"
        git checkout master
        git pull
        popd
    fi
done

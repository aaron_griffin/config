set cmdheight=1
set encoding=utf-8
set termencoding=utf-8
set clipboard=unnamed

if (&term == 'screen.linux') || (&term =~ '^linux')
    set t_Co=8
elseif (&term =~ "-256color")
    set t_Co=256
endif

syntax on
filetype indent plugin on
set modelines=5
set showcmd
set showmode
set autochdir
set autoshelldir

set printoptions=paper:letter,header:0
set diffopt=filler,context:3,vertical

set shell=/bin/sh
set shellpipe=2>&1\|tee
set showbreak=+++
set splitbelow
"will recurse backwards until 'tags' is found
set tags+=tags;

set autowrite
set autoread
set writebackup
set backup
set backupdir=$HOME/.vim/backup/,/tmp
set directory=$HOME/.vim/backup/,/tmp
set undodir=~/.vim/undodir
set undofile
set undolevels=1000
set undoreload=10000

set spelllang=en_us
set spellsuggest=fast,20
" imma commnt with missspellings, use me tu tesst

"bell options (go away, I hate you)
set vb
set t_vb=

" search options
set hlsearch
set incsearch
set ignorecase
set smartcase

"navigation options
set whichwrap=h,l,<,>,[,]
set backspace=indent,eol,start
set tabstop=4
set softtabstop=4
set shiftwidth=4
set textwidth=100
set linebreak
set expandtab
set nowrap
set history=500
set cinoptions=g0,:0,l1,(0,t0
set formatoptions+=l
set selection=inclusive
set mouse=a
set cursorline
set colorcolumn=120
set numberwidth=1
set switchbuf=usetab,newtab

set number
set hidden
set scrolloff=5

"{{{ Folding test
if has("folding")
    set foldenable
    set foldmethod=marker
    set foldmarker={{{,}}}
    set foldcolumn=0
    set foldlevel=100
endif
"}}}

set shortmess=atI
set complete=.,t,i,b,w,k

set wildchar=<tab>
set wildmenu
set wildmode=longest:full,full
set wildignore+=*~,*.o,*.tmp
set completeopt=menu,menuone,longest,preview

" silver searcher greppin
set grepprg=ag\ --vimgrep\ $*
set grepformat=%f:%l:%c:%m

" fzf location
set rtp+=/usr/local/opt/fzf

set previewheight=5

let mapleader = "`"
let &cdpath=','.expand("$HOME")

iab PDB import pdb; pdb.set_trace()
iab BP breakpoint()
iab JSLOG console.log() // eslint-disable-line

"custom filetype stuff
au Syntax {cpp,c,lisp,scheme,python,ruby,javascript} runtime plugin/RainbowParenthesis.vim
au FileType {mail,cvs,svn,git,hg,md} setlocal spell
au FileType qf if &buftype == "quickfix"|setlocal statusline=%-3.3n\ %0*[quickfix]%=%2*\ %<%P|endif
au FileType help setlocal statusline=%-3.3n\ [help]%=\ %<%P
au BufRead,BufNewFile PKGBUILD set ft=bash
"only put the cursorline in the active window
au WinEnter * setlocal cursorline
au WinLeave * setlocal nocursorline
"move to last line we were at and center it
au BufWinEnter *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe 'normal! g`"zz'
  \ | endif
"if num(tabs) > num(spaces), set noet
au BufReadPost *
  \ if len(filter(getbufline(winbufnr(0), 1, "$"), 'v:val =~ "^\\t"')) > len(filter(getbufline(winbufnr(0), 1, "$"), 'v:val =~ "^ "'))
  \ |   setlocal noet
  \ | endif
"Open quickfix when searching and other things
au QuickFixCmdPost [^l]* cwindow
au QuickFixCmdPost l*    lwindow

"highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
au ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$\| \+\ze\t/

"ha! defeated!
nmap q: :q<cr>

" man-page autoreturn after view
nmap K K<cr>

command! -bar -nargs=* -bang W :w<bang> <args>
command! -bar -nargs=* -bang Q :q<bang> <args>
command! -bar -nargs=* -bang Wq :wq<bang> <args>

"NetRW
let g:netrw_keepdir = 1
let g:netrw_winsize = 40
let g:netrw_alto = 1

"Misc syntax
let g:python_highlight_all = 1
let g:ruby_indent_assignment_style = 'variable'

" ALE
let g:ale_fixers = {
\  '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'python': ['black', 'isort'],
\}
let g:ale_linters = {
\   'javascript': ['eslint'],
\   'ruby': ['rubocop'],
\   'python': ['flake8', 'mypy'],
\}
let g:ale_python_auto_pipenv = 1
let g:ale_virtualtext_cursor = 'current'
let g:ale_set_quickfix = 1

"airline
set laststatus=2
let g:airline_extensions = ['branch', 'tabline']
let g:airline_powerline_fonts = 1
let g:airline_theme = 'murmur'

"{{{ Mappings
function MapAllModes(key, cmd)
    let str = a:cmd.'<cr>'
    exec 'noremap <silent> '.a:key.' '.str
    exec 'noremap! <silent> '.a:key.' <c-o>'.str
endfunction
function MapToggle(key, opt)
    call MapAllModes(a:key, ':set '.a:opt.'!')
endfunction
command -nargs=+ MapAllModes call MapAllModes(<f-args>)
command -nargs=+ MapToggle call MapToggle(<f-args>)

MapAllModes <f1> :cwindow
MapAllModes <f2> :ALEFixSuggest
set pastetoggle=<f3>
MapToggle <f4> hlsearch
MapToggle <f5> spell
"}}}

execute pathogen#infect()
execute pathogen#helptags()

" color scheme stuff comes last because of pathogen
set background=dark
let g:zenburn_high_Contrast = 1
let g:inkpot_black_background = 1
colorscheme slate

" -*- vim -*- vim:set ft=vim et sw=4 sts=4:
